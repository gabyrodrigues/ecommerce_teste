require 'rails_helper'

RSpec.describe CategoriasController, type: :controller do


    describe "Categorias should" do
        context "successfully" do
            it "create category with all params" do
                post :create, params: {nome: 'modas'}, as: :json
                expect(response).to have_http_status(201)
            end

            it "list all categories" do
                get :index
                expect(response).to have_http_status(200)
            end

            it "check category name" do
                get :show, params: {id: 5}
                expect(response).not_to be_nil
            end
        end
    end
end
